import { useState, KeyboardEvent, ChangeEvent, useRef, useEffect } from 'react';

type InlineEditProps = {
  initialValue: string;
};
export function InlineEdit(props: InlineEditProps) {
  const { initialValue } = props;
  const editRef = useRef(null);
  const [value, setValue] = useState(initialValue);
  const [inputValue, setInputValue] = useState(initialValue);
  const [isEditing, setIsEditing] = useState(false);

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    setInputValue(e.target.value);
  };

  const handleKeyDown = (e: KeyboardEvent<HTMLInputElement>) => {
    if (e.key === 'Enter') {
      setIsEditing(false);
      setValue(inputValue);
      return;
    }

    if (e.key === 'Escape') {
      handleCancel();
    }
  };

  const handleCancel = () => {
    setIsEditing(false);
    setInputValue(value);
  };

  useEffect(() => {
    if (isEditing) {
      editRef.current.focus();
    }
  }, [isEditing]);

  if (isEditing) {
    return (
      <input
        ref={editRef}
        onChange={handleChange}
        onKeyDown={handleKeyDown}
        onBlur={() => handleCancel()}
        value={inputValue}
      />
    );
  }

  return <div onClick={() => setIsEditing(true)}>{value}</div>;
}
